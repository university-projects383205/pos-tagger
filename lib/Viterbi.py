import numpy as np
import re
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import operator
import copy
from functools import reduce
from lib.Utils import Utils

class Viterbi:
    def __init__(self, is_latin, smooth, p_transition, p_emission):
      self.is_latin = is_latin
      self.tags = Utils.LATIN_TAGS.value if is_latin else Utils.GREEK_TAGS.value
      self.p_transition = p_transition
      self.p_emission = p_emission
      self.words = set(map(lambda x: x[0], self.p_emission.keys()))
      self.smooth = smooth


    def _sanitize_sentence(self, sentence, regexp_index):
      if regexp_index == 0:
        regexp = "[\S]\[--\]"
        offset = -4
      elif regexp_index == 1:
        regexp = "[\S]\.{3}"
        offset = -3
      else:
        regexp = "[^\s.][·.,;?+:]"
        offset = -1

      matches = re.findall(regexp, sentence)

      for match in matches:
        sentence = sentence.replace(match, f"{match[:offset]} {match[offset:]}")

      return sentence.strip()


    def _sanitize_sentence_from_regexp(self, sentence):
        if self.is_latin:
          sentence = self._sanitize_sentence(sentence, 0)
        sentence = self._sanitize_sentence(sentence, 1)
        return self._sanitize_sentence(sentence, 2).split(" ")


    def _get_emission_probabilities(self, emission_key):
      if self.smooth == "ALWAYS_NOUN":
        if emission_key[0] not in self.words:
          return 0 if emission_key[1] == "NOUN" else np.NINF # 0 = np.log(1)
        else:
          return np.NINF if emission_key not in self.p_emission.keys() else self.p_emission[emission_key]
      elif self.smooth == "ALWAYS_NOUN_OR_VERB":
        if emission_key[0] not in self.words:
          return np.log(0.5) if emission_key[1] == "NOUN" or emission_key[1] == "VERB" else np.NINF
        else:
          return np.NINF if emission_key not in self.p_emission.keys() else self.p_emission[emission_key]
      elif self.smooth == "BASED_ON_UNIFORM_DISTRIBUTION":
        if emission_key[0] not in self.words:
          n_of_tags = len(Utils.LATIN_TAGS.value) - 2 if self.is_latin else len(Utils.GREEK_TAGS.value) - 2
          return np.log(1 / n_of_tags)
        else:
          return np.NINF if emission_key not in self.p_emission.keys() else self.p_emission[emission_key]
      elif self.smooth == "BASED_ON_DEV_SET":
        if emission_key[0] not in self.words:
            return np.log(self.tags_dev_set_prob[emission_key[1]]) if self.tags_dev_set_prob[emission_key[1]] != 0 else np.NINF
        else:
          return np.NINF if emission_key not in self.p_emission.keys() else self.p_emission[emission_key]
      else:
        return np.NINF if emission_key not in self.p_emission.keys() else self.p_emission[emission_key]


    def _find_unique_elements(self, a):
      unique = []
      duplicated = False
      tmp = copy.deepcopy(a)
      tmp.sort()

      for i, e in enumerate(tmp[:-1]):
        if e.word == tmp[i + 1].word:
          duplicated = True
        else:
          if not duplicated:
            unique.append(e)
          duplicated = False

      if not duplicated:
        unique.append(tmp[-1])

      return unique


    def smooth_dev_set(self, tokenized_sentences_dev):
      all_wordpos = list(reduce(operator.iconcat, tokenized_sentences_dev, []))
      unique_wordpos = self._find_unique_elements(all_wordpos)
      unique_wordpos_len = len(unique_wordpos)
      tags_count = {t:0 for t in self.tags[1:-1]}

      for wp in unique_wordpos:
        tags_count[wp.tag] += 1

      self.tags_dev_set_prob = {t : tags_count[t] / unique_wordpos_len for t in tags_count}


    def _get_transition_probabilities(self, transition_key):
      return np.NINF if (transition_key) not in self.p_transition.keys() else self.p_transition[transition_key]


    def run(self, sentence):
      # self.sentence = self._sanitize_sentence_from_regexp(sentence) # Serve nel caso di test in cui passiamo a mano due frasi per andare a spaziare i segni di punteggiatura e separarli dai termini vicini
      self.sentence = sentence.split(" ")
      self.viterbi = np.zeros((len(self.tags), len(self.sentence)), dtype=np.float)
      self.backtracking = np.zeros((len(self.tags), len(self.sentence)), dtype=int)
      self.viterbi[0,0] = 0. # Dovrebbe essere viterbi[0, 0] = 1, ma stiamo ragionando in scala logaritmica, e quindi log(1) = 0

      for s in range(1, self.viterbi.shape[0] - 1):
        # Stiamo ragionando in scala logaritmica quindi facciamo una somma piuttosto che una moltiplicazione
        self.viterbi[s, 0] = self._get_transition_probabilities(("START", self.tags[s])) + self._get_emission_probabilities((self.sentence[0], self.tags[s]))
        self.backtracking[s, 0] = 0

      for t in range(1, self.viterbi.shape[1]):
        for s in range(1, self.viterbi.shape[0] - 1): #escludiamo le righe dei tag START e END
          self.viterbi[s, t] = self._max_prob_until(s, t)
          self.backtracking[s, t] = self._backtrack(s, t)

      self.viterbi[self.viterbi.shape[0] - 1, self.viterbi.shape[1] - 1] = self._max_end_transition_prob()
      self.backtracking[self.viterbi.shape[0] - 1, self.viterbi.shape[1] - 1] = self._backtrack(self.viterbi.shape[0] - 1, len(self.sentence))
      tags = self.build_path()

      return tags


    def build_path(self):
      tags = []
      curr_pointer = self.backtracking[self.viterbi.shape[0] - 1][self.viterbi.shape[1] - 1] # Back pointer della colonna finale

      for t in range(self.viterbi.shape[1] - 1, -1, -1):
        tags.append(self.tags[curr_pointer])
        curr_pointer = self.backtracking[curr_pointer][t]
      tags.reverse()
      result = list(zip(self.sentence, tags))

      return result


    def _backtrack(self, s, t):
      max_p = np.NINF
      max_s = -1
      for i in range(1, self.viterbi.shape[0] - 1):
        curr = self.viterbi[i, t - 1] + self._get_transition_probabilities((self.tags[i], self.tags[s]))

        if self.viterbi[i, t - 1] + self._get_transition_probabilities((self.tags[i], self.tags[s])) > max_p:
          max_p = curr
          max_s = i

      return max_s


    def _max_prob_until(self, s, t):
      max_p = np.NINF
      for i in range(1, self.viterbi.shape[0] - 1):
        # Stiamo ragionando in scala logaritmica quindi facciamo una somma piuttosto che una moltiplicazione
        max_p = max(max_p, self.viterbi[i, t - 1] + \
          self._get_transition_probabilities((self.tags[i], self.tags[s])) + \
          self._get_emission_probabilities((self.sentence[t], self.tags[s])))

      return max_p


    def _max_end_transition_prob(self):
      max_p = np.NINF

      for s in range(1, self.viterbi.shape[0] - 1):
        max_p = max(max_p, self.viterbi[s, len(self.sentence) - 1] + self._get_transition_probabilities((self.tags[s], "END")))
      return max_p


    # Funzione inutilizzata, può venir richiamata solo a fini di debug quando si sceglie di eseguire la funzione di test "test_custom_sentence" nel file "../main.py" in cui si cerca di taggare una determinata frase provando a mostrare graficamente il contenuto della matrice "self.viterbi"
    def print_table(self):
      rows = self.tags
      cols = self.sentence

      self.viterbi[self.viterbi == np.NINF] = 800 # Valore scelto arbitrariamente soltanto per creare una "differenza di colore" nella heatmap che si andrà a generare

      frame = pd.DataFrame(self.viterbi, columns=cols, index=rows)
      sns.heatmap(frame, lineWidth=0.5, annot=True)
      plt.show()
