import operator
from functools import reduce
from lib.Utils import Utils
from lib.Utils import timeit


class Baseline:
  def __init__(self, is_latin, tokenized_sentences_train):
    self.tags = Utils.LATIN_TAGS.value if is_latin else Utils.GREEK_TAGS.value
    self.sentences = list(reduce(operator.iconcat, tokenized_sentences_train, []))


  def baseline_learn(self):
    self.words = set(map(lambda x: x.word, self.sentences))
    self.words_tags_frequency = {w:{t:0 for t in self.tags[1:-1]} for w in self.words}

    for sentence_word in self.sentences:
      self.words_tags_frequency[sentence_word.word][sentence_word.tag] += 1

    self.baseline_tags = {w: max(self.words_tags_frequency[w].items(), key=lambda x: x[1])[0] for w in self.words_tags_frequency}


  def run(self, sentence):
    tags = []
    splitted_sentence = sentence.split(" ")

    for w in splitted_sentence:
      tags.append(self.baseline_tags[w] if w in self.baseline_tags.keys() else "NOUN")

    return list(zip(sentence, tags))
