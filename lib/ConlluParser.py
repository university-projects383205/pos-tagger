from collections import namedtuple

class ConlluParser:
    def __init__(self):
        self.WordPos = namedtuple("WordPos", ["word", "tag"])


    def __str__(self):
      return self.tokenized_sentences


    def _read(self, path):
      self.lines = []
      self.tokenized_sentences = []

      with open(path, "r", encoding="UTF-8") as f:
        self.lines = f.readlines()
        if self.lines[-1] != "\n":
          self.lines.append("\n")


    def parse(self, path):
      self._read(path)
      sentences = []

      for line in self.lines:
        if line.startswith("\n"):
          self.tokenized_sentences.append(sentences)
          sentences = []
        elif not line.startswith("#"):
          splittedLine = line.split("\t")
          sentences.append(self.WordPos(splittedLine[1], splittedLine[3]))

      return self.tokenized_sentences


    def get_lines(self):
      return self.lines


    def get_tokenized_sentences(self):
      return self.tokenized_sentences
