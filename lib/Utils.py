from enum import Enum
import time


# Decoratore per cronometrare l'esecuzione di una detrerminata funzione "func"
def timeit(func):
  def timed_func(*args, **kwargs):
    start = time.time()
    func(*args, **kwargs)
    end = time.time()
    print(f"\n[timeit] Function \"{func.__name__}\" took {end - start} seconds to execute\n")

  return timed_func


class Utils(Enum):
  GREEK_TAGS = ["START", "ADJ", "ADP", "ADV", "CCONJ", "DET", "INTJ", "NOUN", "NUM", "PART", "PRON", "PUNCT", "SCONJ", "VERB", "X", "END"]
  LATIN_TAGS = ["START", "ADJ", "ADP", "ADV", "AUX", "CCONJ", "DET", "NOUN", "NUM", "PART", "PRON", "PROPN", "PUNCT", "SCONJ", "VERB", "X", "END"]
  SMOOTH_TYPES = ["ALWAYS_NOUN", "ALWAYS_NOUN_OR_VERB", "BASED_ON_UNIFORM_DISTRIBUTION", "BASED_ON_DEV_SET"]
