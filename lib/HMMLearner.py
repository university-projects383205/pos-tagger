import functools
import operator
import math
import os
import pickle
import numpy as np
from lib.Utils import Utils

class HMMLearner:
  def __init__(self, tokenized_sentences, is_latin):
    self.tags = Utils.LATIN_TAGS.value if is_latin else Utils.GREEK_TAGS.value;
    self.tokenized_sentences = tokenized_sentences
    self.transition_probabilities = {}
    self.emission_probabilities = {}
    self.counted = False


  def _count_tags_occurences(self):
    self.tag_count = {tag:0 for tag in self.tags}
    self.tag_count["START"] = len(self.tokenized_sentences)
    self.tag_count["END"] = len(self.tokenized_sentences)

    for sentence in self.tokenized_sentences:
      for token in sentence:
        self.tag_count[token.tag] += 1

    self.counted = True


  def _learn_transition_probabilities(self):
    if self.counted:
      for t_sentence in self.tokenized_sentences:
        sentence = list(map(lambda x: x.tag, t_sentence))
        previous = "START"
        for i in range(len(sentence) + 1):
          curr = sentence[i] if i < len(sentence) else "END"
          self.transition_probabilities[(previous, curr)] = \
            self.transition_probabilities[(previous, curr)] + 1 if (previous, curr) in self.transition_probabilities else 1
          previous = curr

      for key in self.transition_probabilities:
        self.transition_probabilities[key] = np.log(self.transition_probabilities[key] / self.tag_count[key[0]])


  def _learn_emission_probabilities(self):
    if self.counted:
      for sentence in self.tokenized_sentences:
        for t in sentence:
          self.emission_probabilities[(t.word, t.tag)] = \
            self.emission_probabilities[(t.word, t.tag)] + 1 if (t.word, t.tag) in self.emission_probabilities else 1

      for key in self.emission_probabilities:
        self.emission_probabilities[key] = np.log(self.emission_probabilities[key] / self.tag_count[key[1]])


  def learn_probabilities(self):
    self._count_tags_occurences()
    self._learn_transition_probabilities()
    self._learn_emission_probabilities()


  def get_transition_probabilities(self):
    return self.transition_probabilities


  def get_emission_probabilities(self):
    return self.emission_probabilities
