#!/bin/bash

for conllu_file in $(ls *.conllu); do
  grep -v "^#" "$conllu_file" | tr "\t" "_" | cut -d "_" -f 2,4 | tr "_" "\t" > "${conllu_file%.*}".txt
done

