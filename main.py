#!/usr/bin/python3

from lib.ConlluParser import ConlluParser
from lib.Baseline import Baseline
from lib.HMMLearner import HMMLearner
from lib.Viterbi import Viterbi
from lib.Utils import Utils
from lib.Utils import timeit
import sys
import argparse
import operator
from functools import reduce
import tqdm # Per mostrare le progress bar
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.patches as mpatches


# CREDITS: Link per il PoS Tagger basato su modello MEMM indicato a lezione
# https://github.com/Gan-Tu/ML-DL-NLP/tree/master/MEMM-POS-Tagger
# https://github.com/Gan-Tu/ML-DL-NLP/blob/master/MEMM-POS-Tagger/memm_tagger.py


def tag_sentence(token_sentence, mode):
  sentence = " ".join(list(map(lambda t: t.word, token_sentence)))
  computed_tags = mode.run(sentence)

  return token_sentence, computed_tags


def print_summary(args):
  print(f"Reference language: Latin" if args.latin else f"Reference language: Ancient Greek")
  print(f"Training set path: {args.train_set}")
  print(f"Test set path: {args.test_set}")
  print(f"Development set path: {args.dev_set}" if args.dev_set is not None else f"Development set path not specified")
  print(f"Smoothing technique: {args.smooth.replace('_', ' ').capitalize()}" if args.smooth is not None else f"Smoothing technique not specified")
  print()


def apply_smoothing_technique(args, transition_probabilities, emission_probabilities, tokenized_sentences_test, tokenized_sentences_dev, all_tags):
  viterbi = Viterbi(args.latin, args.smooth, transition_probabilities, emission_probabilities)

  if args.smooth == "BASED_ON_DEV_SET":
    viterbi.smooth_dev_set(tokenized_sentences_dev)

  print_summary(args)
  correct_tags = 0
  viterbi_wrong_original_tags = {}
  # for token_sentence in tokenized_sentences_test: # Variante senza progress bar
  for token_sentence in tqdm.tqdm(tokenized_sentences_test):
    ts, ct = tag_sentence(token_sentence, viterbi)
    correct_tags += len(list(filter(lambda t: t[0].tag == t[1][1], zip(ts, ct))))
    wrong_original_tags = list(map(lambda couple: couple[0].tag, filter(lambda t: t[0].tag != t[1][1], zip(ts, ct))))
    for t in wrong_original_tags:
      viterbi_wrong_original_tags[t] = 1 if t not in viterbi_wrong_original_tags.keys() else viterbi_wrong_original_tags[t] + 1
  print()
  print(f"Annotated tags that are not correctly computed: {dict(sorted(viterbi_wrong_original_tags.items(), key=lambda x: x[1], reverse=True))}\n")
  print()

  return correct_tags / all_tags * 100


def compare_smoothing_techniques(args, transition_probabilities, emission_probabilities, tokenized_sentences_test, tokenized_sentences_dev, baseline_accuracy, all_tags):
  smoothing_accuracies = {smooth_type:0 for smooth_type in ["NO_SMOOTHING"] + Utils.SMOOTH_TYPES.value}

  for type in ["NO_SMOOTHING"] + Utils.SMOOTH_TYPES.value:
    args.smooth = None if type == "NO_SMOOTHING" else type
    smoothing_accuracies[type] = apply_smoothing_technique(args, transition_probabilities, emission_probabilities, tokenized_sentences_test, tokenized_sentences_dev, all_tags)

  smoothing_techniques = list(map(lambda x: x.replace("_", " ").capitalize(), smoothing_accuracies.keys()))
  accuracies = list(smoothing_accuracies.values())
  smoothing_techniques.reverse()
  accuracies.reverse()

  plot_accuracies(smoothing_techniques, accuracies, baseline_accuracy)


def plot_accuracies(smoothing_techniques, accuracies, baseline_accuracy, font_size=14):
  # Ritorna il valore massimo più il relativo indice
  def custom_max(t1, t2):
    x = max(t1[1], t2[1])
    return (t1[0], x) if x == t1[1] else (t2[0], x)

  colors = ["#269AFF" for i in smoothing_techniques]
  max_accuracy_index = reduce(custom_max, enumerate(accuracies))[0]
  colors[max_accuracy_index] = "#0038FF"

  plt.barh(smoothing_techniques, accuracies, color=colors)
  plt.axvline(baseline_accuracy, color="#001F46")
  plt.xlim(0, 100)

  xticks = list(plt.xticks()[0])
  plt.xticks(xticks + [baseline_accuracy])
  for i, t in enumerate(list(plt.xticks()[1])[:-1]):
    t.set_text(int(xticks[i]))
  list(plt.xticks()[1])[-1].set_text(f"{baseline_accuracy:.2f}")
  list(plt.xticks()[1])[-1].set_ha("center")
  list(plt.xticks()[1])[-1].set_bbox({"facecolor": "red", "alpha": 0.8})
  plt.xticks(list(plt.xticks()[0]), list(plt.xticks()[1]), fontsize=font_size)
  plt.yticks(fontsize=font_size)

  for i, accuracy in enumerate(accuracies):
    plt.text(accuracy, i, f"{accuracy:.2f} %", ha="center", bbox={"facecolor": "red", "alpha": 0.8}, fontsize=font_size)

  plt.title("Accuracies related to different smoothing techniques", fontdict={"fontweight": "bold", "fontsize": font_size})
  plt.xlabel("Accuracy", fontsize=font_size)
  # plt.ylabel("Smoothing technique") # Etichetta data all'asse delle ordinate non necessaria
  plt.grid(linestyle="--", axis="x", alpha=0.8)
  plt.legend(handles=[mlines.Line2D([0], [0], color="#001F46", label="Baseline accuracy"), mpatches.Patch(color="#0038FF", label="Best accuracy")], loc=1, fontsize=font_size)
  plt.show()


def main(args):
  if args.latin:
    if args.train_set is None:
      args.train_set = "data/la_llct-ud-train.conllu"

    if args.test_set is None:
      args.test_set = "data/la_llct-ud-test.conllu"
  else:
    if args.train_set is None:
      args.train_set = "data/grc_perseus-ud-train.conllu"

    if args.test_set is None:
      args.test_set = "data/grc_perseus-ud-test.conllu"

  parser = ConlluParser()
  tokenized_sentences_train = parser.parse(args.train_set)
  tokenized_sentences_test = parser.parse(args.test_set)
  tokenized_sentences_dev = parser.parse(args.dev_set) if args.smooth == "BASED_ON_DEV_SET" else None

  # Numero totale di tag
  all_tags = reduce(operator.iadd, map(lambda t_sentence: len(t_sentence), tokenized_sentences_test), 0)

  baseline = Baseline(args.latin, tokenized_sentences_train)
  baseline.baseline_learn()
  correct_tags = 0
  baseline_wrong_original_tags = {}

  for token_sentence in tokenized_sentences_test:
    ts, ct = tag_sentence(token_sentence, baseline)
    correct_tags += len(list(filter(lambda t: t[0].tag == t[1][1], zip(ts, ct))))
    wrong_original_tags = list(map(lambda couple: couple[0].tag, filter(lambda t: t[0].tag != t[1][1], zip(ts, ct))))

    for t in wrong_original_tags:
      baseline_wrong_original_tags[t] = 1 if t not in baseline_wrong_original_tags.keys() else baseline_wrong_original_tags[t] + 1

  baseline_accuracy = correct_tags / all_tags * 100
  print(f"Baseline accuracy: {baseline_accuracy:.2f} %\n")
  print(f"Annotated tags that are not correctly computed: {dict(sorted(baseline_wrong_original_tags.items(), key=lambda x: x[1], reverse=True))}\n")

  learner = HMMLearner(tokenized_sentences_train, args.latin)
  learner.learn_probabilities()
  transition_probabilities = learner.get_transition_probabilities()
  emission_probabilities = learner.get_emission_probabilities()

  if args.compare:
    tokenized_sentences_dev = parser.parse(args.dev_set)
    compare_smoothing_techniques(args, transition_probabilities, emission_probabilities, tokenized_sentences_test, tokenized_sentences_dev, baseline_accuracy, all_tags)
  else:
    # test_custom_sentence(transition_probabilities, emission_probabilities)

    accuracy = apply_smoothing_technique(args, transition_probabilities, emission_probabilities, tokenized_sentences_test, tokenized_sentences_dev, all_tags)
    print(f"Viterbi accuracy: {accuracy:.2f} %")


# Funzione inutilizzata, può venir richiamata solo a fini di debug. Si fa il tagging di una determinata frase per cercare di mostrare graficamente la matrice risultante dalla fase di decoding
def test_custom_sentence(transition_probabilities, emission_probabilities):
  sentence = "ὑμεῖς δέ , ὦ Κύνουλκε, τὰς διηγήσεις ταύτας , κἂν ψευδεῖς ὦσιν , ἀληθεῖς εἶναι πιστεύετε , καὶ τὰ τοιαῦτα τῶν ποιημάτων ἃ περὶ τοὺς παιδικούς ἐστιν ἔρωτας ἰδίως μελετᾶτε ..."
  viterbi = Viterbi(False, "ALWAYS_NOUN_OR_VERB", transition_probabilities, emission_probabilities)
  viterbi.run(sentence)
  viterbi.print_table()


if __name__ == "__main__":
  try:
    argparser = argparse.ArgumentParser()
    lang_meg = argparser.add_mutually_exclusive_group(required=True)
    lang_meg.add_argument("-l", "--latin", help="set Latin as reference language", action="store_true")
    lang_meg.add_argument("-g", "--greek", help="set Greek as reference language", action="store_true")
    argparser.add_argument("-trs", "--train-set", help="specify the training set to use", type=str, default=None)
    argparser.add_argument("-tes", "--test-set", help="specify the test set to use", type=str, default=None)
    argparser.add_argument("-des", "--dev-set", help="specify the development set to use for BASED_ON_DEV_SET smoothing technique", type=str, default=None)
    smooth_meg = argparser.add_mutually_exclusive_group()
    smooth_meg.add_argument("-s", "--smooth", help="apply a specified smoothing technique", type=str, default=None, choices=Utils.SMOOTH_TYPES.value)
    smooth_meg.add_argument("-c", "--compare", help="compare all the accuracies from all the possible smoothing techniques", action="store_true")
    args = argparser.parse_args()

    if (args.dev_set is None and args.smooth == "BASED_ON_DEV_SET") or (args.dev_set is None and args.compare):
      argparser.error("argument --dev-set: expected one argument")

    main(args)
  except KeyboardInterrupt:
    print()
    sys.exit(1)
